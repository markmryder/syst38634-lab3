package phones;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runners.*;

public class TestSmartPhone 
{
	
//	 public SmartPhone(String givenName, double givenPrice, double givenVersion)
//	 {
//	  name = givenName;
//	  price = givenPrice;
//	  version = givenVersion;
//	 }
	
	
	@Test
	public void testGetFormattedPriceGoodInput() 
	{
		SmartPhone phone = new SmartPhone("phone", 12.00, 1.0);
		System.out.println(phone.getFormattedPrice());
		assertTrue("improper format", phone.getFormattedPrice().equals("$12"));
	}
	
	@Test
	public void testGetFormattedPriceBadInput() 
	{
		SmartPhone phone = new SmartPhone("phone", 12.45636, 1.0);
		System.out.println(phone.getFormattedPrice());
		assertTrue("improper format", phone.getFormattedPrice().equals("$12.456"));
	}
	
	
	@Test
	public void testSetVersionNoInput() 
	{
		SmartPhone phone = new SmartPhone();
		double version = phone.getVersion();
		System.out.println(version);
		assertTrue("Version is incorrect", version == 0.0);
	}
	@Test
	public void testSetVersionNormalInput() throws Exception
	{
		SmartPhone phone = new SmartPhone();
		phone.setVersion(3.0);
		double version = phone.getVersion();
		assertTrue("Version is incorrect", version == 3.0);
	}
	@Test(expected = Exception.class)
	public void testSetVersionOverMaxValue() throws Exception
	{
		SmartPhone phone = new SmartPhone();
		phone.setVersion(5.0);
	}
	
}
