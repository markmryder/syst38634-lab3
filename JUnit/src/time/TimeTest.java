package time;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runners.*;

public class TimeTest {

//	private String timeToTest;
//	
//	public TimeTest()
//	{
//		
//	}
//	
//	public TimeTest(String dataTime) 
//	{
//		timeToTest = dataTime;
//	}
//	
//	@Parameterized.Parameters
//	
//	public static Collection<Object[]> loadData ()
//	{
//		Object[][] data = {{"12:05:05"}};
//		return Arrays.asList(data);
//	}
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testGetTotalSeconds( ) 
	{
		int seconds = Time.getTotalSeconds("12:05:05");
		assertTrue("The seconds were not calculated properly", seconds==43505);
	}
//	@Test
//	public void testGetTotalSecondsMaxData() 
//	{
//		int seconds = Time.getTotalSeconds("99:59:59");
//		assertTrue("The seconds were not calculated properly", seconds==35999);
//	}
	@Test(expected = Exception.class)
	public void testGetTotalSecondsStringInput()
	{
		int seconds = Time.getTotalSeconds("improper data");
	}
	@Test(expected = Exception.class)
	public void testGetTotalSecondsImproperInput()
	{
		int seconds = Time.getTotalSeconds("123456");
	}
	
	
	@Test
	public void testGetMilliseconds() throws Exception
	{
		int milliseconds = Time.getMilliseconds("12:05:05:005");
		
		assertTrue("Milliseconds are wrong", milliseconds == 5);
	}
	@Test
	public void testGetMillisecondsBoundaryOut()
	{
		int milliseconds = Time.getMilliseconds("12:05:05:1000");
		
		assertTrue("Milliseconds are wrong", milliseconds == 100);
	}
	
	@Test
	public void testGetMillisecondsBoundaryIn()
	{
		int milliseconds = Time.getMilliseconds("12:05:05:999");
		
		assertTrue("Milliseconds are wrong", milliseconds == 999);
	}
	
	@Test(expected = Exception.class)
	public void testGetMillisecondsException() throws NumberFormatException
	{
		int milliseconds = Time.getMilliseconds("12:05:05:abc");
	}



}
