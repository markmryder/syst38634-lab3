package password;

public class Password {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public static boolean validatePasswordDigits(String password) 
	{
		String regexDigits = "^(?=.*?\\d.*\\d)(?=.*?[A-Za-z])[a-zA-Z0-9]{8,}$";
		if(!password.matches(regexDigits)) 
		{
			return false;
		}
		return true;

	}
	
	public static boolean validatePasswordLength(String password) 
	{
		if(password.length() < 8) 
		{
			return false;
		}
		return true;
	}

}
