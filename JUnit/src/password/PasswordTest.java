package password;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PasswordTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	

	@Test
	public void testPasswordLengthException() 
	{
		String password = "abc";
		assertFalse("Must have 8 characters", Password.validatePasswordLength(password));
	}	
	@Test
	public void testPasswordLengthBoundaryIn() 
	{
		String password = "abcdefgh";
		assertTrue("Must be 8 character", Password.validatePasswordLength(password));
	}
	@Test
	public void testPasswordLengthBoundaryOut() 
	{
		String password = "abcdefg";
		assertFalse("Must be 8 character", Password.validatePasswordLength(password));
	}
	@Test
	public void testPasswordLength() //Happy Path 
	{
		String password = "abcdefghijk";
		assertTrue("Must be 8 character", Password.validatePasswordLength(password));
	}	
	
	@Test
	public void testPasswordDigitsException() 
	{
		String password = "abcdefg";
		assertFalse("Not enough digits", Password.validatePasswordDigits(password));
	}
	@Test
	public void testPasswordDigitsBoundaryIn() 
	{
		String password = "abcdefg12";
		assertTrue("Not enough digits", Password.validatePasswordDigits(password));
	}
	@Test
	public void testPasswordDigitsBoundaryOut() 
	{
		String password = "abcdefg1";
		assertFalse("Not enough digits", Password.validatePasswordDigits(password));
	}
	@Test
	public void testPasswordDigits() 
	{
		String password = "abcdefg12345";
		assertTrue("Not enough digits", Password.validatePasswordDigits(password));
	}
	
	


}
